//
//  SearchView.swift
//  SheetView
//
//  Created by shreejwal giri on 14/06/2021.
//

import SwiftUI


struct SearchData: Identifiable {
    let id = UUID()
    let title: String
    let coverimage: String
}

struct SearchView: View {
    @State var searchText = ""
    var searchListArr = [
        SearchData(title: "movie one", coverimage: "search-one"),
        SearchData(title: "movie two", coverimage: "search-two"),
        SearchData(title: "movie one", coverimage: "search-one"),
        SearchData(title: "movie two", coverimage: "search-two"),
        SearchData(title: "movie one", coverimage: "search-one"),
        SearchData(title: "movie two", coverimage: "search-two"),
        SearchData(title: "movie one", coverimage: "search-one"),
        SearchData(title: "movie two", coverimage: "search-two"),
        SearchData(title: "movie one", coverimage: "search-one"),
        SearchData(title: "movie two", coverimage: "search-two"),
        SearchData(title: "movie one", coverimage: "search-one"),
        SearchData(title: "movie two", coverimage: "search-two"),
        
    ]
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: true, content: {
            VStack(alignment: .leading, spacing: 20, content: {
                HStack(alignment: .center, spacing: 10, content: {
                    Image(systemName: "magnifyingglass")
                        .netflixIconStyle(frameSize: 20)
                    TextField("Search for  a show, movies, genre etc.", text: $searchText)
                })
                
                ForEach (searchListArr.filter { $0.title.localizedCaseInsensitiveContains(searchText) || searchText.isEmpty}) { searchList in
                    VStack(alignment: .leading, spacing: 0, content: {
                        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            Image(searchList.coverimage)
                                .resizable()
                                .frame(width: 150, height: 100, alignment: .leading)
                                .cornerRadius(5)
                            Text(searchList.title)
                                .netflixStyle()
                            Spacer()
                            Image(systemName: "play.circle")
                                .netflixIconStyle(frameSize: 30)
                                .foregroundColor(.white)
                        })
                        .cornerRadius(/*@START_MENU_TOKEN@*/3.0/*@END_MENU_TOKEN@*/)
                    })
                    .padding(.trailing, 20)
                    .background(Color.netflixColor)
                }
            })
            
        })
        .background(Color.primaryBGColor)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
