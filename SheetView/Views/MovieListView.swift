//
//  MovieListView.swift
//  SheetView
//
//  Created by shreejwal giri on 12/06/2021.
//

import SwiftUI

struct MovieContentView: View {
    
    let myListArr = [
        MovieList(coverimage: "coverImage-one"),
        MovieList(coverimage: "coverimage-two"),
        MovieList(coverimage: "coverImage-one"),
        MovieList(coverimage: "coverimage-two"),
        MovieList(coverimage: "coverImage-one"),
        MovieList(coverimage: "coverimage-two"),
        MovieList(coverimage: "coverImage-one"),
        MovieList(coverimage: "coverimage-two")
    ]
    
    let trendingNowArr = [
        MovieList(coverimage: "birdBox"),
        MovieList(coverimage: "coverimage-two"),
        MovieList(coverimage: "birdBox"),
        MovieList(coverimage: "coverimage-two")
    ]
    
    let tvShowArr = [
        MovieList(coverimage: "birdBox"),
        MovieList(coverimage: "coverimage-two"),
        MovieList(coverimage: "birdBox"),
        MovieList(coverimage: "birdBox"),
        MovieList(coverimage: "coverImage-one")
    ]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16, content: {
            MovieSection(sectionTitle: "My List", movieList: myListArr)
            MovieSection(sectionTitle: "Trending Now", movieList: trendingNowArr)
            MovieSection(sectionTitle: "TV Shows", movieList: tvShowArr)
            MovieSection(sectionTitle: "Get It On The Action", movieList: myListArr)
            MovieSection(sectionTitle: "Exciting TV Show", movieList: trendingNowArr)
            MovieSection(sectionTitle: "Top Pick For Shreejwal", movieList: tvShowArr)
        })
        .padding(.leading, 16)
        .foregroundColor(.white)
    }
}

struct MovieSection: View {
    @State var sectionTitle: String
    @State var movieList: [MovieList]
    
    var body: some View {
        Text(sectionTitle)
            .fontWeight(.bold)
            .font(.system(size: 20))
        
        ScrollView(.horizontal, showsIndicators: false, content: {
            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10, content: {
                ForEach(movieList) { movies in
                    Image(movies.coverimage ?? "")
                        .netflixCoverStyle()
                }
            })
        })
        Spacer()
    }
}

