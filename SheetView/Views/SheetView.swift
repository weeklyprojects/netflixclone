//
//  SheetView.swift
//  SheetView
//
//  Created by shreejwal giri on 12/06/2021.
//

import SwiftUI

struct SheetView: View {
    @State var geometry: CGSize
    @Binding var showSheetView: Bool
    var body: some View {
        ZStack {
            VStack {
                Spacer()
                ZStack(alignment: .center, content: {
                    Rectangle()
                        .fill(Color(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)))
                        .frame(width: geometry.width, height: geometry.height / 2.5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .clipShape(TopRoundedShape())
                    
                    VStack(alignment: .leading, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                        
                        HStack(alignment: .top, spacing: 12, content: {
                            Image("sweetTooth")
                                .netflixCoverStyle()
                            VStack(alignment: .leading, spacing: 10, content: {
                                
                                VStack(alignment: .leading, spacing: 0, content: {
                                    Text("Sweet Tooth")
                                        .netflixStyle(size: 25, fontWeight: .bold)
                                    
                                    HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10, content: {
                                        Text("2021")
                                        Text("16+")
                                        Text("movie")
                                    })
                                    .foregroundColor(.gray)
                                })
                                
                                Text("On a perilous adventure in a post-apocalyptic world, a boy who's half-human and half deer searches for a new beginning with a gruff protector.")
                                    .fontWeight(.regular)
                                    .font(.system(size: 14))
                                
                            })
                            .foregroundColor(.white)
                        })
                        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        .padding(.top, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        
                        Button(action: {
                        }, label: {
                            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                                Image(systemName: "play.fill")
                                Text("Play")
                            })
                            .foregroundColor(.black)
                        })
                        .frame(width: 180, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .background(Color.white)
                        .cornerRadius(5)
                        .padding(.leading, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        .padding(.bottom, 40)
                    })
                    
                    Image(systemName: "xmark")
                        .netflixIconStyle(frameSize: 18)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                        .offset(x: geometry.width / 2 - 20, y: -geometry.height / 5.5)
                        .foregroundColor(.white)
                })
            }
            
        }
        .shadow(radius: 4)
        .edgesIgnoringSafeArea(.bottom)
        .onTapGesture {
            withAnimation(.linear(duration: 1.5)) {
                showSheetView = false
            }
        }
    }
}


struct SheetView_Previews: PreviewProvider {
    static var previews: some View {
        SheetView(geometry: CGSize(width: 380, height: 500), showSheetView: .constant(true))
    }
}
