//
//  ContentView.swift
//  SheetView
//
//  Created by shreejwal giri on 05/06/2021.
//

import SwiftUI
import Combine

struct MovieList: Identifiable {
    let id = UUID()
    let coverimage: String?
}

struct HomeView: View {
    @State var showSheetView: Bool
    @State var barItemChange: Bool
    @State var isScrolledToTop: Bool
    
    let detector: CurrentValueSubject<CGFloat, Never>
    let publisher: AnyPublisher<CGFloat, Never>
    
    init() {
        let detector = CurrentValueSubject<CGFloat, Never>(0)
        self.publisher = detector
            .debounce(for: .seconds(0), scheduler: DispatchQueue.main)
            .dropFirst()
            .eraseToAnyPublisher()
        self.detector = detector
        
        showSheetView   = false
        barItemChange   = false
        isScrolledToTop = false
    }
    
    var body: some View {
        GeometryReader { (geometry) in
            NavigationView {
                ZStack(alignment: .top, content: {
                    Color.primaryBGColor.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
                        VStack(alignment: .leading, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                            ZStack(alignment: .top, content: {
                                CoverImage(geometry: geometry)
                                    .onTapGesture {
                                        withAnimation(.easeInOut(duration: 1)) {
                                            showSheetView.toggle()
                                        }
                                    }
                                
                                TopView()
                                    .padding([.leading, .trailing], 16)
                                    .padding(.top, 40)
                                
                            })
                            MovieContentView()
                        })
                        
                        .background(
                            GeometryReader {
                                Color.clear.preference(key:  ViewOffsetKey.self,
                                                       value: -$0.frame(in: .named("scroll")).origin.y)
                            }
                        )
                        .onPreferenceChange(ViewOffsetKey.self) { detector.send($0) }
                        
                    })
                    .edgesIgnoringSafeArea(.top)
                    .coordinateSpace(name: "scroll")
                    .onReceive(publisher) {
                        print("Stopped on: \($0)")
                        if $0 <= 100 {
                            isScrolledToTop = true
                        }
                        isScrolledToTop = $0 <= 100 ? true : false
                        
                        
                        if isScrolledToTop {
                            withAnimation(.linear(duration: 0.2)) {
                                barItemChange = false
                            }
                        } else {
                            withAnimation(.linear(duration: 0.2)) {
                                barItemChange = true
                            }
                        }
                    }
                    
                    VStack{
                        Spacer()
                        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 50, content: {
                            TitleButton(title: "TV Show") {
                            }
                            TitleButton(title: "Movies") {
                            }
                            TitleButton(title: "My List") {
                            }
                        })
                        .padding([.leading, .trailing], 8)
                        .padding(.bottom, 20)
                    }
                    .frame(width: geometry.size.width, height: 140, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .background(barItemChange ? Color.black.opacity(0.7) : Color.clear)
                    .offset(x: 0, y: barItemChange ? -30 : 0)
                    .edgesIgnoringSafeArea(.top)
                    
                    
                    SheetView(geometry: geometry.size, showSheetView: $showSheetView)
                        .offset(x: 0, y: showSheetView ? 0 : geometry.size.height)
                })
                .edgesIgnoringSafeArea(.bottom)
                .navigationBarHidden(true)
            }
            .preferredColorScheme(.dark)
        }
    }
}

struct CoverImage: View {
    @State var geometry: GeometryProxy
    var body: some View {
        Image("sweetTooth")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: geometry.size.width, height: geometry.size.height / 1.4, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .overlay(
                Color.black.opacity(0.5)
            )
            .ignoresSafeArea(.all, edges: .top)
    }
}

struct TopView: View {
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10, content: {
            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Image("netflix-logo")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30)
                })
                
                Spacer()
                
                HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 20, content: {
                    Button(action: {}, label: {
                        NavigationLink(destination: SearchView()) {
                            Image(systemName: "magnifyingglass")
                                .netflixIconStyle(frameSize: 20)
                        }
                        .navigationBarHidden(true)
                    })
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Image("netflix-face")
                            .netflixIconStyle(frameSize: 30, cornerRadius: 3)
                    })
                })
                
            })
            .foregroundColor(.white)
            Spacer()
        })
    }
}

struct TitleButton: View {
    var title: String
    var action: (() -> Void)
    var body: some View {
        Button(action: {
            action()
        }, label: {
            Text(title)
                .netflixStyle()
        })
    }
}

struct ViewOffsetKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat.zero
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value += nextValue()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

