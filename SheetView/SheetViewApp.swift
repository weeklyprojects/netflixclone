//
//  SheetViewApp.swift
//  SheetView
//
//  Created by shreejwal giri on 05/06/2021.
//

import SwiftUI

@main
struct SheetViewApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
