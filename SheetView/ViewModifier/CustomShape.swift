//
//  CustomShape.swift
//  SheetView
//
//  Created by shreejwal giri on 05/06/2021.
//

import SwiftUI

struct TopRoundedShape: Shape {
    var roundedCorner: CGFloat = 16
    func path(in rect: CGRect) -> Path {
        let roundedPath = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: roundedCorner, height: roundedCorner))
        return Path(roundedPath.cgPath)
    }
}
