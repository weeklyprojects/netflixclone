//
//  CustomContent.swift
//  SheetView
//
//  Created by shreejwal giri on 12/06/2021.
//

import SwiftUI

extension Text {
    func netflixStyle(size: CGFloat? = 15, color: Color? = .white, fontWeight: Font.Weight? = .semibold) -> some View  {
        self.foregroundColor(color!)
            .fontWeight(fontWeight!)
            .font(.system(size: size!))
    }
}

extension Image {
    func netflixIconStyle(frameSize: CGFloat, cornerRadius: CGFloat? = 0) -> some View {
        self.resizable()
            .frame(width: frameSize, height: frameSize, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .cornerRadius(cornerRadius!)
    }
    
    func netflixCoverStyle(cornerRadius: CGFloat? = 8) -> some View {
        self.resizable()
            .frame(width: 120, height: 180, alignment: .leading)
            .aspectRatio(contentMode: .fit)
            .cornerRadius(cornerRadius!)
    }
}
