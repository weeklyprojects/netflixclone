//
//  CustomColor.swift
//  SheetView
//
//  Created by shreejwal giri on 05/06/2021.
//

import SwiftUI

extension Color {
    static var primaryBGColor = Color(.black)
    static var netflixColor = Color(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1))
}
