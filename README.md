# MyPortfolio

# MyPortfolio

Clone of Netflix App Home page.

- Contains 
    - [x] Bar Animation
    - [x] Bottom Sheet Animation
    - [x] Use of ScrollView and Stacks
    - [x] Use of Navigation View
    - [x] Use of Search Bar, Text Field
    - [x] Use of Custom Classes and more. 


<img src="netflixOne.gif" width="250.81" height="523" />
<img src="NetflixTwo.gif" width="250.81" height="523" />
<img src="netflixthree.gif" width="250.81" height="523" />
<img src="netflixfour.gif" width="250.81" height="523" />


